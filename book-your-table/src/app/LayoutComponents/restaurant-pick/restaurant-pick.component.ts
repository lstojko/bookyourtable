import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RestaurantsService } from '../../Services/RestaurantsService/restaurants.service';


@Component({
  selector: 'app-restaurant-pick',
  templateUrl: './restaurant-pick.component.html',
  styleUrls: ['./restaurant-pick.component.scss'],
  providers: []
})
export class RestaurantPickComponent implements OnInit {
  @Output() openRestaurantModel: EventEmitter<any> = new EventEmitter<any>();
  showNavigationArrows = true;
  showNavigationIndicators = false;
  restaurantsPick = [];
  private openRestaurantDetailsValue: boolean = false;
  constructor(public restaurantsService: RestaurantsService) {
    this.getRestaurants();
  }

  ngOnInit() {
  }

  openRestaurantDetails(){
    this.openRestaurantDetailsValue = true;
    this.openRestaurantModel.emit(this.openRestaurantDetailsValue);
  }

  getRestaurants(){
    this.restaurantsPick = this.restaurantsService.getRestaurants();
  }

}
